package it.unibg.so.lab3;


public class User extends Thread {
	
	private CokeMachine machine;
	
	public User(CokeMachine m){
		machine = m;
	}

	@Override
	public void run() {
		while (true) {
			SleepUtilities.sleep();

			System.out.println("User wants to consume.");
			Coke c = machine.consume();
			System.out.println("User consumed: " + c);
		}
	}

}
