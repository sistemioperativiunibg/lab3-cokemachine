package it.unibg.so.lab3;

public class Driver {
	
	public static void main(String[] args){
		CokeMachine buffer = new CokeMachine(5);

        Thread operator = new Operator(buffer);
        Thread user = new User(buffer);

        operator.start();
        user.start();
	}
}
